/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "Manager.hpp"
#include "SNHost.hpp"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFDynamicLayout.hpp>
#include <DFApplication.hpp>

DesQ::SNI::Manager::Manager() {
    for ( QScreen *scrn: qApp->screens() ) {
        createInstance( scrn );

        QVariantAnimation *showAnim = createAnimator( mSurfaces[ scrn->name() ], mInstances[ scrn->name() ], true );
        showAnim->start();
        mInstanceStates[ scrn->name() ] = true;
    }
}


DesQ::SNI::Manager::~Manager() {
}


void DesQ::SNI::Manager::handleMessages( QString mesg, int fd ) {
    if ( mesg.startsWith( "reload" ) ) {
    }

    else if ( mesg.startsWith( "raise" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( (parts.count() >= 2) ) {
            raiseInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "lower" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( (parts.count() >= 2) ) {
            lowerInstance( parts[ 1 ] );
        }
    }

    else if ( mesg.startsWith( "toggle" ) ) {
        QStringList parts = mesg.split( "\n" );

        if ( mInstanceStates[ parts[ 1 ] ] ) {
            hideLayerSurface( parts[ 1 ] );
            qApp->messageClient( "hidden", fd );
        }

        else {
            showLayerSurface( parts[ 1 ] );
            qApp->messageClient( "shown", fd );
        }
    }

    else {
        qDebug() << "Invalid command\n";
        return;
    }
}


void DesQ::SNI::Manager::createInstance( QScreen *scrn ) {
    StatusNotifierHost *sni = new StatusNotifierHost();

    sni->setStyleSheet( "#base {background: rgba(0, 0, 0, 0.5); border: 1px solid gray; border-radius: 5px; }" );
    sni->setLayoutStyle( StatusNotifierHost::Grid );

    sni->setMinimumWidth( 32 );
    sni->setMaximumWidth( 270 );
    sni->setMinimumHeight( 32 );
    sni->setButtonSize( 32 );
    sni->setMargins( QMargins( 3, 3, 3, 3 ) );

    sni->setWindowFlags( sni->windowFlags() | Qt::BypassWindowManagerHint );
    sni->show();

    if ( WQt::Utils::isWayland() ) {
        WQt::LayerShell::LayerType lyr     = WQt::LayerShell::Bottom;
        wl_output                  *output = WQt::Utils::wlOutputFromQScreen( scrn );
        WQt::LayerSurface          *cls    = wlRegistry->layerShell()->getLayerSurface( sni->windowHandle(), output, lyr, "desq-sni" );

        cls->setAnchors( WQt::LayerSurface::Top | WQt::LayerSurface::Left );

        /** Size of our surface */
        cls->setSurfaceSize( sni->size() );

        /** We'll keep a gap of 18 in on the left and top */
        cls->setMargins( QMargins( -sni->size().width(), 18, 0, 0 ) );

        /** Move this for panel and other */
        cls->setExclusiveZone( 0 );

        /** We may need keyboard interaction. */
        cls->setKeyboardInteractivity( WQt::LayerSurface::OnDemand );

        /** Commit to our choices */
        cls->apply();

        QObject::connect(
            sni, &StatusNotifierHost::resized, [ sni, cls ](){
                cls->setSurfaceSize( sni->size() );
                cls->apply();
            }
        );

        mInstances[ scrn->name() ]      = sni;
        mSurfaces[ scrn->name() ]       = cls;
        mSurfaceStates[ scrn->name() ]  = lyr;
        mInstanceStates[ scrn->name() ] = true;
    }

    else {
        sni->close();
        delete sni;
    }
}


void DesQ::SNI::Manager::destroyInstance( QString opName ) {
    if ( mSurfaces.contains( opName ) ) {
        WQt::LayerSurface *surf = mSurfaces.take( opName );
        delete surf;
    }

    if ( mSurfaceStates.contains( opName ) ) {
        mSurfaceStates.remove( opName );
    }

    if ( mInstances.contains( opName ) ) {
        StatusNotifierHost *w = mInstances.take( opName );
        w->close();
        delete w;
    }

    if ( mInstanceStates.contains( opName ) ) {
        mInstanceStates.remove( opName );
    }
}


void DesQ::SNI::Manager::raiseInstance( QString opName ) {
    if ( not mSurfaces.contains( opName ) ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Top ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Top;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::SNI::Manager::lowerInstance( QString opName ) {
    if ( not mSurfaces.contains( opName ) ) {
        return;
    }

    if ( mSurfaceStates[ opName ] == WQt::LayerShell::Bottom ) {
        return;
    }

    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );
    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    connect(
        hideAnim, &QVariantAnimation::finished, [ = ] () {
            mSurfaceStates[ opName ] = WQt::LayerShell::Bottom;

            mSurfaces[ opName ]->setLayer( WQt::LayerShell::Bottom );
            mSurfaces[ opName ]->apply();
        }
    );

    QSequentialAnimationGroup *grp = new QSequentialAnimationGroup();

    grp->addAnimation( hideAnim );
    grp->addAnimation( showAnim );

    grp->start();
    mInstanceStates[ opName ] = true;
}


void DesQ::SNI::Manager::showLayerSurface( QString opName ) {
    if ( not mInstances.contains( opName ) ) {
        return;
    }

    mSurfaceStates[ opName ] = WQt::LayerShell::Top;

    mSurfaces[ opName ]->setLayer( WQt::LayerShell::Top );
    mSurfaces[ opName ]->apply();

    QVariantAnimation *showAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], true );

    showAnim->start();

    mInstanceStates[ opName ] = true;
}


void DesQ::SNI::Manager::hideLayerSurface( QString opName ) {
    QVariantAnimation *hideAnim = createAnimator( mSurfaces[ opName ], mInstances[ opName ], false );

    hideAnim->start();

    mInstanceStates[ opName ] = false;
}


QVariantAnimation * DesQ::SNI::Manager::createAnimator( WQt::LayerSurface *surf, StatusNotifierHost *ui, bool show ) {
    QVariantAnimation *anim = new QVariantAnimation();

    anim->setDuration( 500 );

    if ( show ) {
        anim->setStartValue( -ui->width() - 18 );
        anim->setEndValue( 18 );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::OutCubic ) );
    }

    else {
        anim->setStartValue( 18 );
        anim->setEndValue( -ui->width() - 18 );
        anim->setEasingCurve( QEasingCurve( QEasingCurve::InOutQuart ) );
    }

    connect(
        anim, &QVariantAnimation::valueChanged, [ = ]( QVariant val ) {
            surf->setMargins( QMargins( val.toInt(), 18, 0, 0 ) );
            surf->apply();

            qApp->processEvents();
        }
    );

    return anim;
}
