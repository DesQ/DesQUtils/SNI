/**
 * This file is a part of DesQ SNI.
 * DesQ SNI is a Widget to show SN Icons for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SNHost.hpp"
#include <QDebug>
#include <QtDBus>
#include <QApplication>

// Wayland Headers
#include <DFDynamicLayout.hpp>

#define WATCHER_SERVICE_KDE    "org.kde.StatusNotifierWatcher"
#define WATCHER_SERVICE_FDO    "org.freedesktop.StatusNotifierWatcher"
#define WATCHER_OBJECT         "/StatusNotifierWatcher"
#define WATCHER_PATH_KDE       "org.kde.StatusNotifierWatcher"
#define WATCHER_PATH_FDO       "org.freedesktop.StatusNotifierWatcher"

StatusNotifierHost::StatusNotifierHost( LayoutStyle lytStyle, QWidget *parent ) : QWidget( parent ) {
    mLytStyle = lytStyle;

    QString dbusName = QString( "org.kde.StatusNotifierHost-%1-%2" ).arg( QApplication::applicationPid() ).arg( 1 );

    if ( !QDBusConnection::sessionBus().registerService( dbusName ) ) {
        qDebug() << QDBusConnection::sessionBus().lastError().message();
    }

    mWatcherKDE = new QDBusInterface( WATCHER_SERVICE_KDE, WATCHER_OBJECT, WATCHER_PATH_KDE, QDBusConnection::sessionBus() );
    mWatcherKDE->call( "RegisterStatusNotifierHost", dbusName );

    mWatcherFDO = new QDBusInterface( WATCHER_SERVICE_FDO, WATCHER_OBJECT, WATCHER_PATH_FDO, QDBusConnection::sessionBus() );
    mWatcherFDO->call( "RegisterStatusNotifierHost", dbusName );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_KDE,
        WATCHER_OBJECT,
        WATCHER_PATH_KDE,
        "StatusNotifierItemRegistered",
        "s",
        this,
        SLOT( itemAdded( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_KDE,
        WATCHER_OBJECT,
        WATCHER_PATH_KDE,
        "StatusNotifierItemUnregistered",
        "s",
        this,
        SLOT( itemRemoved( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_FDO,
        WATCHER_OBJECT,
        WATCHER_PATH_FDO,
        "StatusNotifierItemRegistered",
        "s",
        this,
        SLOT( itemAdded( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE_FDO,
        WATCHER_OBJECT,
        WATCHER_PATH_FDO,
        "StatusNotifierItemUnregistered",
        "s",
        this,
        SLOT( itemRemoved( QString ) )
    );

    sniLyt = new DFL::DynamicLayout();
    sniLyt->setAlignment( Qt::AlignLeft );
    sniLyt->setHorizontalSpacing( mItemSpacing );
    sniLyt->setVerticalSpacing( mItemSpacing );
    sniLyt->setContentsMargins( mMargins );

    QWidget *base = new QWidget( this );

    base->setObjectName( "base" );
    base->setLayout( sniLyt );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( base );
    setLayout( lyt );

    QVariant registeredKDE = mWatcherKDE->property( "RegisteredStatusNotifierItems" );
    QVariant registeredFDO = mWatcherFDO->property( "RegisteredStatusNotifierItems" );

    for ( QString sni: registeredKDE.toStringList() ) {
        itemAdded( sni );
    }

    for ( QString sni: registeredFDO.toStringList() ) {
        itemAdded( sni );
    }

    setWindowFlags( Qt::Window | Qt::FramelessWindowHint );
    setAttribute( Qt::WA_TranslucentBackground );

    setButtonSize( mSize );
}


StatusNotifierHost::~StatusNotifierHost() {
    delete mWatcherKDE;
    delete mWatcherFDO;
}


void StatusNotifierHost::setButtonSize( int size ) {
    mSize = size;

    /* Resize the buttons */
    for ( QToolButton *btn: findChildren<QToolButton *>() ) {
        btn->setFixedSize( size, size );
        btn->setIconSize( QSize( size - 4, size - 4 ) );
    }

    int width  = 0;
    int height = 0;

    switch ( mLytStyle ) {
        /* Width keeps increasing */
        case Horizontal: {
            width  = mServices.count() * size + mMargins.left() + mMargins.right() + mItemSpacing * (mServices.count() - 1);
            height = size + mMargins.top() + mMargins.bottom();
            break;
        }

        /* Height keeps increasing */
        case Vertical: {
            width  = size + mMargins.left() + mMargins.right();
            height = mServices.count() * size + mMargins.top() + mMargins.bottom() + mItemSpacing * (mServices.count() - 1);
            break;
        }

        /* Fixed width, height changes */
        case Grid: {
            int cell = size + mItemSpacing;
            width = rect().width();
            int availWidth = width - mMargins.left() - mMargins.right();
            int cols       = floor( availWidth / cell );
            int rows       = ceil( 1.0 * mServices.count() / cols );

            height = rows * size + mMargins.top() + mMargins.bottom() + mItemSpacing * (rows - 1);
            break;
        }
    }

    setFixedSize( width, height );
    sniLyt->setGeometry( QRect( 0, 0, width, height ) );

    emit resized();
}


void StatusNotifierHost::setLayoutStyle( LayoutStyle lytStyle ) {
    mAutoLayout = false;

    mLytStyle = lytStyle;
    setButtonSize( mSize );
}


void StatusNotifierHost::setItemSpacing( int itmSpc ) {
    mItemSpacing = itmSpc;
    sniLyt->setHorizontalSpacing( itmSpc );
    sniLyt->setVerticalSpacing( itmSpc );
    setButtonSize( mSize );
}


void StatusNotifierHost::setMargins( QMargins margins ) {
    mMargins = margins;
    sniLyt->setContentsMargins( margins );
    setButtonSize( mSize );
}


QSize StatusNotifierHost::minimumSizeHint() const {
    if ( mLytStyle == Horizontal ) {
        return QSize( sniLyt->count() * mSize, mSize );
    }

    else if ( mLytStyle == Vertical ) {
        return QSize( mSize, sniLyt->count() * mSize );
    }

    else {
        return sniLyt->sizeHint();
    }
}


void StatusNotifierHost::itemAdded( QString serviceAndPath ) {
    if ( mServices.contains( serviceAndPath ) ) {
        return;
    }

    int                  slash   = serviceAndPath.indexOf( '/' );
    QString              serv    = serviceAndPath.left( slash );
    QString              path    = serviceAndPath.mid( slash );
    StatusNotifierButton *button = new StatusNotifierButton( serv, path, this );

    mServices.insert( serviceAndPath, button );
    sniLyt->addWidget( button );
    button->show();

    setButtonSize( mSize );
}


void StatusNotifierHost::itemRemoved( QString serviceAndPath ) {
    StatusNotifierButton *button = mServices.value( serviceAndPath, nullptr );

    if ( button ) {
        button->deleteLater();
        sniLyt->removeWidget( button );

        mServices.remove( serviceAndPath );
    }

    setButtonSize( mSize );
}


void StatusNotifierHost::resizeEvent( QResizeEvent *rEvent ) {
    /* Resize the widget */
    rEvent->accept();

    if ( mAutoLayout ) {
        /*
         * Deduce the layout style
         * Fixed Width -> Vertical Layout
         * Fixed Height -> Horizontal Layout
         * Otherwise -> Grid Layout
         */

        int minHeight = minimumHeight();
        int maxHeight = maximumHeight();

        int minWidth = minimumWidth();
        int maxWidth = maximumWidth();

        /* Fixed Height */
        if ( minHeight == maxHeight ) {
            mLytStyle = StatusNotifierHost::Horizontal;
            setButtonSize( minHeight );
        }

        /* Fixed Width */
        else if ( minWidth == maxWidth ) {
            mLytStyle = StatusNotifierHost::Vertical;
            setButtonSize( minWidth );
        }

        else {
            mLytStyle = StatusNotifierHost::Grid;
            setButtonSize( mSize );
        }
    }
}
