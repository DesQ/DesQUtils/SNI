/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "Manager.hpp"

#include <signal.h>
#include <desq/desq-config.h>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFApplication.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

WQt::Registry *wlRegistry = nullptr;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/SNI.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application *app = new DFL::Application( argc, argv );

    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "SNI" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-sni.desktop" );

    app->interceptSignal( SIGSEGV, true );
    app->interceptSignal( SIGABRT, true );
    app->interceptSignal( SIGTERM, true );
    app->interceptSignal( SIGQUIT, true );
    app->interceptSignal( SIGINT,  true );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "s", "raise" }, "Raise the sni-widget to the top" } );
    parser.addOption( { { "l", "lower" }, "Lower the sni-widget to the bottom" } );
    parser.addOption( { { "t", "toggle" }, "Show/hide the sni-widget instance" } );
    parser.addOption( { { "o", "output" }, "Output on which raise/lower/toggle is done.", "output" } );

    parser.process( *app );

    /** If the panel is already running, bug out */
    if ( app->isRunning() ) {
        qDebug() << "SNI is already running";

        if ( parser.isSet( "raise" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which SNI instance is to be raised.";
                return 1;
            }

            app->messageServer( "raise\n" + parser.value( "output" ) );
        }

        else if ( parser.isSet( "lower" ) ) {
            if ( not parser.isSet( "output" ) ) {
                qCritical() << "Please specify the output on which SNI instance is to be lowered.";
                return 1;
            }

            app->messageServer( "lower\n" + parser.value( "output" ) );
        }

        return 0;
    }

    if ( not app->lockApplication() ) {
        qDebug() << "Unable to lock this instance. Aborting...";
        return 1;
    }

    /** Wayland Registry */
    wlRegistry = new WQt::Registry( WQt::Wayland::display() );
    wlRegistry->setup();

    DesQ::SNI::Manager *sniMgr = new DesQ::SNI::Manager();

    QObject::connect( app, &DFL::Application::messageFromClient, sniMgr, &DesQ::SNI::Manager::handleMessages );

    QObject::connect(
        app, &QApplication::screenAdded, [ sniMgr ] ( QScreen *screen ) {
            sniMgr->createInstance( screen );
        }
    );

    return app->exec();
}
