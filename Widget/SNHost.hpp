/**
 * This file is a part of DesQ SNI.
 * DesQ SNI is a Widget to show SN Icons for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QDir>
#include <QWidget>

#include <desqui/ShellPlugin.hpp>

#include "SNButton.hpp"

namespace DFL {
    class DynamicLayout;
}

class StatusNotifierHost : public QWidget {
    Q_OBJECT

    public:
        enum LayoutStyle {
            Horizontal = 0x458651,
            Vertical,
            Grid,
        };

        StatusNotifierHost( LayoutStyle lytStyle = Grid, QWidget *parent = nullptr );
        ~StatusNotifierHost();

        /* Resize the buttons and the widget */
        void setButtonSize( int );

        /* Set the layout style */
        void setLayoutStyle( LayoutStyle lytStyle );

        /* Set the spacing between the buttons */
        void setItemSpacing( int );

        /* Set the margins for the widget */
        void setMargins( QMargins );

        /* Necessary but comfortable size of the widget */
        QSize minimumSizeHint() const;

    public slots:
        void itemAdded( QString serviceAndPath );
        void itemRemoved( QString serviceAndPath );

    private:
        QDBusInterface *mWatcherKDE;
        QDBusInterface *mWatcherFDO;
        QHash<QString, StatusNotifierButton *> mServices;

        DFL::DynamicLayout *sniLyt;
        LayoutStyle mLytStyle = Horizontal;

        bool mAutoLayout = true;

        int mItemSpacing = 0;
        QMargins mMargins;
        int mSize = 32;

    protected:
        void resizeEvent( QResizeEvent * ) override;

    Q_SIGNALS:
        void resizeNeeded();
        void resized();
};
