# DesQ SNI
Status Notifier Interface for DesQ. This project provides SN Watcher daemon that registers the Status icons, and SNI executable
that displays the status icons via Wlroots Layer Surface. A plugin is provided for use with Panel/Dock etc.


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* DBusMenu Qt5
* WlrootsQt (https://gitlab.com/desktop-frameworks/wayqt.git)
* LibDesQ (https://gitlab.com/DesQ/libdesq.git)
* LibDesQ UI (https://gitlab.com/DesQ/libdesqui.git)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/SNI.git DesQSNI`
- Enter the `DesQSNI` folder
  * `cd DesQSNI`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.2
* DBusMenu Qt5      0.9.3

### Known Bugs
* Popups are shown as top-level surfaces

### Upcoming
* Any other feature you request for... :)
