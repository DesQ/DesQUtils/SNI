/**
 * This file is a part of DesQ SNI.
 * DesQ SNI is a Widget to show SN Icons for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Plugin.hpp"
#include "SNHost.hpp"

/* Name of the plugin */
QString SNIPlugin::name() {
    return "DesQ SNI";
}


/* Name of the plugin */
QIcon SNIPlugin::icon() {
    return QIcon::fromTheme( "desq" );
}


/* The plugin version */
QString SNIPlugin::version() {
    return QString( PROJECT_VERSION );
}


/* The SNI plugin */
DesQ::Shell::PluginWidget *SNIPlugin::widget( QWidget *parent ) {
    DesQ::Shell::PluginWidget *plugin = new DesQ::Shell::PluginWidget( parent );
    QHBoxLayout               *lyt    = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );
    lyt->addWidget( new StatusNotifierHost( StatusNotifierHost::Horizontal, plugin ) );
    plugin->setLayout( lyt );

    return plugin;
}
