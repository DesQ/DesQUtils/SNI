/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This example demonstrates how to write a simple status notifier
 * watcher daemon. Be sure to have DFL::SNI library installed
 **/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <DFStatusNotifierWatcher.hpp>

bool CheckService( DFL::StatusNotifierWatcher *sniw, QString service ) {
    /* Some one else has taken this service!! Grrr... */
    if ( not sniw->isServiceRunning() ) {
        qWarning() << QString( "Unable to start the service 'org.%1.StatusNotifierWatcher'." ).arg( service ).toUtf8().data();
        qWarning() << "Please ensure that another instance of this app is not running elsewhere.";
        return false;
    }

    /* Something went wrong... */
    else if ( not sniw->isObjectRegistered() ) {
        qWarning() << "Unable to register the object '/StatusNotifierWatcher'.";
        qWarning() << "Please ensure that another instance of this app is not running elsewhere.";
        return false;
    }

    /* All is well. */
    else {
        qInfo() << QString( "Service 'org.%1.StatusNotifierWatcher' running..." ).arg( service ).toUtf8().data();
        qInfo() << "Object /StatusNotifierWatcher registered...";
        qInfo() << "Ready for incoming connections...";
    }

    return true;
}


int main( int argc, char *argv[] ) {
    QCoreApplication app( argc, argv );

    app.setOrganizationName( "DFL" );
    app.setApplicationName( "StatusNotifier Watcher" );
    app.setApplicationVersion( PROJECT_VERSION );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    parser.addOption( { { "d", "daemon" }, "Run this application as a daemon", "" } );
    parser.addOption( { "no-daemon", "Don't run this application as a daemon", "" } );

    QCommandLineOption servOpt( QStringList() << "service", "Service to be started (kde or fdo)", "service", "kde" );
    parser.addOption( servOpt );

    parser.process( app );

    /** Create a fork for the KDE version of the StatusNotifierWatcher */
    pid_t pid = fork();

    /** Child process */
    if ( pid == 0 ) {
        DFL::StatusNotifierWatcher *sniw;

        DFL::StatusNotifierWatcher *sniwK = DFL::StatusNotifierWatcher::createInstance( "kde" );

        /** Failed to launch this service: Check the logs */
        if ( CheckService( sniwK, "kde" ) == false ) {
            return EXIT_FAILURE;
        }

        DFL::StatusNotifierWatcher *sniwF = DFL::StatusNotifierWatcher::createInstance( "freedesktop" );

        /** Failed to launch this service: Check the logs */
        if ( CheckService( sniwF, "freedesktop" ) == false ) {
            return EXIT_FAILURE;
        }

        /** Run the main loop */
        return app.exec();
    }

    /** Fork failed */
    else if ( pid < 0 ) {
        qCritical() << "Failed to fork().";
        // Exit the program
        exit( EXIT_FAILURE );
    }

    /** This is the parent process */
    else {
        /** waitpid() if --no-daemon is set */
        if ( parser.isSet( "no-daemon" ) == true ) {
            waitpid( pid, nullptr, 0 );
        }

        /** daemon: close the std io descriptors and close the parent */
        else {
            int devNull = open( "/dev/null", O_RDWR );

            dup2( devNull, STDIN_FILENO );
            dup2( devNull, STDOUT_FILENO );
            dup2( devNull, STDERR_FILENO );
            close( devNull );

            /** Don't wait for child processes to finish */
            exit( EXIT_SUCCESS );
        }
    }

    return app.exec();
}
